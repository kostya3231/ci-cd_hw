import { getDayOfWeekOfDate } from '../src/utils';

describe('getDayOfWeekOfDate', () => {
  it('returns correct day', () => {
    const date = new Date(2023, 6, 22);
    const day = 'Saturday';

    const result = getDayOfWeekOfDate(date);

    expect(result).toEqual(day);
  });
});
