import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';

import { getDayOfWeekOfDate } from './utils';

dotenv.config();

const app: Express = express();
const port = process.env.PORT;

app.get('/', (req: Request, res: Response) => {
  res.send(`Today: ${getDayOfWeekOfDate(new Date())}`);
});

app.listen(port, () => {
  console.log(`Server started at http://localhost:${port}`);
});
